\chapter{Results and evaluation}
\label{chapter5}
\thispagestyle{empty}

\begin{quotation}
{\footnotesize
\noindent \emph{Software is getting slower more rapidly than hardware becomes faster.}
\begin{flushright}
Wirth's Law
\end{flushright}
}
\end{quotation}
\vspace{0.5cm}
\null
\vfill
\noindent This chapter discusses the experimental realizations and the evaluation of this work.
The first section presents two of the applications we have developed using SPF. 
On the basis of the scenarios outlined in Chapter \ref{chapter3}, a thorough description of these applications helps to understand how the functionality of SPF can be used to build social proximity applications.
The second section provides an assessment on the code quality achievable by using SPF compared to other communication middleware. 
Here we compare four different configuration of a social proximity application on the basis of code quality metrics and architectural components.
It helps to understand how a higher level of abstraction can ease the development of proximity oriented application.
Finally, the third section analyzes the impact and limitations of the inter-process architecture of SPF. 
To do this, we compare the response time of a network transaction according to different communication means and payload sizes.



\newpage
\section{Samples of applications}\label{sec:ch5-developed-apps}

\subsection{SPFChatDemo}
SPFChatDemo is an Android application that enables proximity-based social interactions, according to the SNiP scenario described in section \ref{sec:ch3-application-scenarios}. In particular, SPFChatDemo offers the following functions:

\begin{description}
	\item[People discovery]: The application enables the discovery of people in proximity, displaying the list of found results. Users can also specify a set of properties that the profile of results must match. Once a new person is discovered, the app allows further social interactions.
	\item[Profile Visualization]: Users can view the profile of other people in proximity.
	\item[Greeting]: Users in proximity can exchange \emph{greets}, instant interactions that results in a toast notification displayed on the phone of the target, if the app is in use, or in a system notification otherwise.
	\item[Chat]: A user can start a conversation in real time with a person discovered in proximity by sending a new text message. Incoming messages are notified by means of system notifications. 
\end{description}

These functions have been implemented relying on the capabilities offered by the SPF Framework. The first function, People Discovery, is built upon the Search API. In particular, the application creates a SPF query that matches all SPF instances that have SPFChatDemo installed. The user can further customize this query by adding property and tag parameters through the UI of the application. When the user starts the discovery process, the application registers a search in SPF providing the query, a fixed search configuration and a callback. This callback is used to update the list of discovered instances visible to the user: each entry displays the person's name, and two buttons, one for the greet interaction, and one to send a chat message. If no people has been discovered once the process finishes, a message is displayed to the user.

The user can display the profile of a person by clicking on its entry in the list of discovered people. This features displays a predefined subset of the available profile fields: to view the full profile of a person, the user can open it in the SPF Provider application. The values of profile fields are obtained from the remote instance using the SPF Profile API. Therefore, the visibility of field values depends on the clearance set by the remote user, thus not all fields may be accessible. 

The Greeting and Chat functions are built upon the Services API using activities. In particular, SPFChatDemo uses the inter-operable verbs \texttt{greet} and \texttt{message} to exchange greets and messages, respectively. The consumption of these activities is implemented by \texttt{ProximityService}, a SPF Service with two methods annotated as activity consumers, one for greets, and one for messages.

The implementation of the service is defined by means of the \texttt{ProximityServiceImpl} class. It features a pluggable strategy that enables the customization of the business logic that handles incoming activities. Incoming greets are directly dispatched to the currently selected strategy, while chat messages are persisted in a database, then dispatched to the strategy.

The default reaction strategy dispatches a system notification every time a greet or chat message is received. In this way, if the application is not running and SPF binds to the service implementation to deliver an activity, the default strategy is used, thus resulting in a system notification. On the other hand, when the application is active, the default strategies are overridden by components of the user interface, in order to provide real-time updates. For example, the list of chat conversation and the list of messages in a conversation are automatically updated when a message is received.

\subsection{SPFCouponing}
SPFCouponing is a couple of Android applications that enable the proximity-based distribution of digital coupons tailored to the social profile of shop customers. These applications realize the Targeted Advertising scenario, described in section \ref{sec:ch3-targeted-advertising}. These applications are based on the concept of \emph{Coupon}, which is a special offer for products of a given category, gifted to a customer by a shop owner. Each coupon features a title, a text message and a photo. To determine if a coupon is tailored to a given customer, the Provider application matches the public social profile with the product category of the coupon: if the profile contains the category, then probably the customer is interested in related coupons. For example, if the profile of a user contains the ``Smartphones'' term, then he probably is interested in receiving coupons related to novel Android devices.

The first application, named \emph{SPFCouponingProvider}, is targeted at shop owners and provides the following functions:

\begin{itemize}
	\item \textbf{Coupon Configuration}: Shop owners can configure the coupons that are delivered to interested clients.
	\item \textbf{Category Advertising}: Shop owners can select which product categories should be added to the social profile and thus advertised to clients.
	\item \textbf{Welcome message}: Shop owners can configure a text message that is delivered to all nearby customers upon the first visit.
\end{itemize}

The second application, named \emph{SPFCouponingClient}, is targeted at customers and interacts with SPFCouponingProvider to receive coupons targeted to the user. The functions offered by this application are:

\begin{itemize}
	\item \textbf{Coupon browsing}: Customers can browse the list of coupons received from nearby shops.
	\item \textbf{Category Selection}: Customers can select which product categories they are interested in: in this way, they will receive a notification every time a shop selling a favorite category is nearby. Customers can also opt-in to receive coupons for each category they select.
\end{itemize}

The interaction between the Provider and the Client applications is implemented leveraging only on the capabilities offered by SPF; in particular, the Notification, Services and Profile API are used.

\subsubsection*{Coupon Delivery}
As previously described, digital coupons are created by shop owners using the Provider application. When the owner defines a new coupon, the application registers a new SPF trigger to react to customers interested in the coupon, as described in figure \ref{fig:ch5-coupon-delivery}. The query of this trigger matches SPF instances that have the client application installed, and that contain the coupon category name as \emph{tag}. The action specified by the trigger is an IntentAction, whose intent activates the delivery of the coupon to the client application. Once the Trigger is registered, the provider application stores the Coupon information, including the coupon detail (title, text, photo and category) and the trigger ID, into a database.

\begin{figure}[ht]
	\centering
	\includegraphics[width=\linewidth]{./pictures/chapter5/ch5-coupon-delivery}
	\caption{Coupon delivery}
	\label{fig:ch5-coupon-delivery}
\end{figure}

The delivery of coupons to client applications is implemented by an Android \emph{IntentReceiver}, activated by the intents dispatched by SPF when coupon triggers are fired. These intents contain the id of the trigger that caused the intent: with this piece of information, the receiver obtains the original coupon from the database. The actual delivery of the coupon is implemented by means of an SPF service, called \texttt{CouponDeliveryService}, which is exposed by client applications. Using the identifier of the remote instance contained in the intent, the receiver obtains a reference to the remote SPF instance and executes the service method to deliver the coupon to the client.

The CouponingDeliveryService, exposed by Client applications, contains only one method, named \texttt{deliverCoupon}. The service implementation features a pluggable strategy approach, similar to the one of SPFChatDemo. After received coupons are saved in a database, the implementation delegates the reaction to a strategy that can be set from other application components. When no external strategy is set, the default one is executed, which causes a system notification to be displayed to the user. External behaviors, on the other hand, are set from GUI components to implement in-app reaction to new coupons. In particular, if the user is currently browsing available coupons, the list will update automatically.

\subsubsection*{Shop Notification}
As previously described, the client application lets customers select the product categories in which they are interested. After a category is added, the user will receive notifications when a shop offering a favorite category is nearby and, possibly, coupons for selected product categories. Since the coupon delivery system is based on triggers, when the user selects to receive coupons for a given category, the provider application adds it to the \emph{interests} field of the social profile.

The shop matching behavior is based on SPF triggers as well. When the customer adds a new category to their favorites, the application creates a new SPF trigger, whose query matches SPF instances that have the provider application installed, and that contain the product category as \emph{tag}. Similarly to coupon delivery, the trigger specifies an intent action whose intents activate an \texttt{IntentReceiver}. In this case, the receiver is designed to dispatch system notifications when intents are received. These notifications, once activated, display the social profile of the shop.
 
\subsubsection*{Welcome Message}
The welcome message function allows shop owners to define a message, composed by a title and a body, that is dispatched to customers the first time they are nearby the shop. The delivery of messages rely on the SPF Notification API as well: when the welcome message is activated, the Provider application registers a SPF trigger that matches all instances with the Client application installed. The action of the trigger is an \texttt{ActionSendNotification} that dispatches the welcome message to the remote instance within the SPF Provider application. The trigger is configured as one-shot, so that it is fired only once for each customer.

\section{Code quality}\label{sec:ch5-code-quality}
One of the main objectives of SPF is easing the development of social proximity applications.
The complexities of the development of such services arise from the intricacies of the underlying networking technologies and the absence of high level abstractions. 
Moreover, specific constraints comes from the peculiar nature of mobile software components, that add to the developer the burden of dealing with background components, multi-threading and synchronization issues, as well as a potentially complex management of resources.
All these issues force the developer to design the whole infrastructure, spending his efforts in the engineering of the communication layers, rather than focusing on the main functions of the application.

To understand and assess the effectiveness of SPF, we decided to analyze the code-base of a proximity based chat implemented in four different configuration that use or do not use the SPF. 
The first comparison we propose compares the Wi-Fi Direct chat, bundled in the Android SDK samples, against the same application but with the networking layer substituted by SPF. 
The second analysis is between the AllJoyn chat provided within the SDK and a SPF Chat sample.

To evaluate the code of the different applications we used SonarQube\footnote{http://www.sonarqube.org/}, an open source platform to measure code quality. 
We focused on two important metrics: the overall lines of code, and the cyclomatic complexity, which counts the number of different paths in the source code and provides a quantitative measure of the complexity of the code. 
A high cyclomatic complexity is not an issue per se, as it depends on the size of the program. 
However, it can be used to compare two applications that implement the same functionality, since a less complex implementation is easier to debug and maintain.

\subsection*{Wi-Fi Direct and SPF}
The Wi-Fi Direct chat sample we considered is the one provided within the Android SDK\footnote{samples/android-17/WiFiDirectServiceDiscovery}. 
It is a simple app that allows two devices to exchange messages.
When the app is launched, it uses the service discovery capabilities of Wi-Fi Direct to provide a list of available peers. 
This implies the local registration of the service and the start of a discovery procedure. 
Once one of the user has selected a service, a Wi-Fi Direct connection is performed.
 
According to the result of the negotiation, two different subclasses of Thread are started.
If the device is a group owner, it instantiates a \texttt{GroupOwnerSocketHandler} that holds a pool of thread for incoming socket connections.
If the device is a standard peer, the \texttt{ClientSocketHandler} is used to establish a socket connection and then running another thread to read from the socket.
The component for writing and reading from the socket is shared between the two cases and it is called \texttt{ChatManager}.
The decoupling between the IO threads and the UI one is implemented with an Android Handler, that allows to deliver messages in the main thread.

To substitute Wi-Fi Direct with SPF, we defined a simple SPF service for the reception of textual messages and used the SPF Search API to perform the discovery of peer in proximity. 

Tables \ref{tab:ch5-wfd-spf-code-size} and \ref{tab:ch5-wfd-spf-code-complexity} shows respectively the data about the size of the code and its cyclomatic complexity. 
As expected the introduction of SPF and its higher level of abstraction, led to a considerable decrease in the overall cyclomatic complexity and code-base size. 
\begin{table}[ht!]
	\centering
	\begin{tabular}{lcc}
		& \multicolumn{1}{l}{\textbf{WfdChat}} & \multicolumn{1}{l}{\textbf{WfdChat on SPF}} \\ \hline
		\textit{Lines of Code} & 619                                     & 301                                  \\
		\textit{N\textsuperscript{\underline{o}} of Files}          & 8                                        & 5                                    \\
		\textit{N\textsuperscript{\underline{o}} of Classes}       & 12                                       & 9                                   \\
		\textit{N\textsuperscript{\underline{o}} of Functions}     & 32                                       & 32    \\ \hline                              
	\end{tabular}
	\caption{Wi-Fi Direct Chat code-base size}
	\label{tab:ch5-wfd-spf-code-size}
\end{table}

\begin{table}[ht!]
	\centering
	\begin{tabular}{lcc}
		\multicolumn{1}{l}{\textbf{Complexity}} & \multicolumn{1}{l}{\textbf{WfdChat}} & \multicolumn{1}{l}{\textbf{WfdChat on SPF}} \\ \hline
		\textit{cc/function}                       & 3                                      & 1.6                                  \\
		\textit{cc/class}                          & 8.1                                     & 5.7                                  \\
		\textit{cc/file}                           & 12.1                                     & 10.2                                 \\
		\textit{cc total}                          & 97                                      & 51                                  \\ \hline
	\end{tabular}
	\caption{Wi-Fi Direct Chat cyclomatic complexity}
	\label{tab:ch5-wfd-spf-code-complexity}
\end{table}



\subsection*{AllJoyn and SPF}
For the second analysis we considered the chat bundled in the Alljoyn SDK as a sample.
Since its logic structure is very tied to the capabilities of the AllJoyn framework, we were not able to provide a such precise comparison as in the Wi-Fi Direct case: in fact we compared the AllJoyn demo app with an our application called \emph{SPFChat} that provides a similar functionality. 
Despite that, as we will discuss later, it was still interesting to compare the SPF design with another similar high-level framework.

AllJoynChat allows a group of devices to exchange messages between each other. 
A user may decide to create a \emph{channel}, which represent a standard chat room.
To create a channel the application register and advertise a well-known name which is composed by a predefined prefix and a suffix that depends on the name that the user assigned to the channel.
Remote clients may use the same application to search for available channels and join a chat room.
The concept of group is implemented by means of multi-point sessions and bus signals.
Each user of the application has to register a bus object whose interface declares a single bus signal that allows the broadcasting of textual messages.
The advertised well-known name is used as an endpoint to create multi-point session. Once a user has joined a session, i.e., a channel, he can receive and send bus signals that are delivered to all the users within the established session.
An Android foreground Service is used to keep the connections opened and to hold the AllJoyn resources.

Differently from AllJoynChat, SPFChat does not allow the creation of chat rooms, but offers a more usual interaction. 
Users of SPFChat may discover other peers in proximity and start a conversation. 
As for the Wi-Fi Direct case, we used SPF search and one SPF service to implement respectively the discovery and the chat function. Moreover, conversations are stored persistently in a SQLite database. 


Code size and cyclomatic complexity are shown respectively in tables \ref{tab:ch5-alljoyn-spf-code-size} and \ref{tab:ch5-alljoyn-spf-code-complexity}.
\begin{table}[ht]
	\centering
	\begin{tabular}{lcc}
		& \multicolumn{1}{l}{\textbf{AllJoynChat}} & \multicolumn{1}{l}{\textbf{SPFChat}} \\ \hline
		\textit{Lines of Code} & 1392                                     & 712                                  \\
		\textit{N\textsuperscript{\underline{o}} of File}          & 9                                        & 8                                    \\
		\textit{N\textsuperscript{\underline{o}} of Classes}       & 16                                       & 15                                   \\
		\textit{N\textsuperscript{\underline{o}} of Functions}     & 95                                       & 63        \\ \hline                          
	\end{tabular}
	\caption{SPFChat and AllJoynChat code-base size}
	\label{tab:ch5-alljoyn-spf-code-size}
\end{table}

\begin{table}[ht]
	\centering
	\begin{tabular}{lcc}
		\multicolumn{1}{l}{\textbf{Complexity}} & \multicolumn{1}{l}{\textbf{AllJoynChat}} & \multicolumn{1}{l}{\textbf{SPFChat}} \\ \hline
		\textit{cc/function}                       & 2.3                                      & 1.9                                  \\
		\textit{cc/class}                          & 14.8                                     & 8.1                                  \\
		\textit{cc/file}                           & 26.2                                     & 15.3                                 \\
		\textit{cc total}                          & 236                                      & 122                                  \\ \hline
	\end{tabular}
	\caption{SPFChat and AllJoynChat code-base cyclomatic complexity}
	\label{tab:ch5-alljoyn-spf-code-complexity}
\end{table}

The major differences between the two application do not regard the different complexities of the offered functions. 
In fact, the programming model for group communication offered by AllJoyn is almost the same of the one offered by spf services, except for the concept of multi-point session.
The real difference about AllJoynChat is inherent in how resources are handled.
These are mainly managed by the foreground Android Service in a class called \texttt{AllJoynService}, that holds a reference to the AllJoyn bus attachment along with the state of the network. 
To allow the application to access the middleware, it offers a complex multi-threading structure that handles operations and  asynchronous events. 
These characterize the life-cycle and the interactions between the bus attachment and its application. 
On the other hand, SPF takes care of all these problems by providing a simpler abstraction that automatically handle the network resources and, more important, is tied to the life-cycle of the Android components. 
As result, SPFChat does not need a foreground service, since  this feature is already supported by the SPF framework.


\section{Performance}\label{sec:ch5-performance}
We then evaluated how the introduction of the SPF affected the performance of the apps that rely on it. 
The experiments aimed at collecting the response times of different implementations of the same service/app that differ for the adopted communication layer.

More in detail, we created a ping service that sends requests and waits for acknowledgments from the receiver. 
The different sizes of the payload were obtained by providing a string obtained by concatenating a varying number of characters. 
We collected the response times on two Samsung Galaxy S4 smartphones: one acting as server and the other one as client. 
The client phone was in charge of performing service invocations, one after another, by varying the size of the payload. 
The experiments considered five different configurations that differed in the used communication middleware: SPF- AllJoyn, SPF-WiFi Direct, Alloyn alone, WiFi Direct alone, and a local-only implementation, that is, a configuration that only used inter-process communications on the same device without remote invocations.

Figure \ref{fig:ch5-resptime} shows the results of these experiments. 
\begin{figure}[ht]
	\centering
	\begin{subfigure}[b]{\linewidth}
		\includegraphics[width=\textwidth]{pictures/chapter5/ch5-resptime-spf-allj}
		\label{fig:ch5-resptime-spf-aj}
		\subcaption{SPF and AllJoyn}
	\end{subfigure}
	\quad
	\begin{subfigure}[b]{\linewidth}
		\includegraphics[width=\textwidth]{pictures/chapter5/ch5-resptime-spf-wfd}
		\label{fig:ch5-resptime-spf-wfd}
		\subcaption{SPF and Wi-Fi Direct}
	\end{subfigure}
	\caption{Response time comparison with .95 confidence level}
	\label{fig:ch5-resptime}
\end{figure}
If we compare the response time of a local transaction with the one of a remote service invocation, as expected, the impact of inter-process communication is negligible with respect to the dimension of a network transaction. 
If we analyze the results with and without the SPF, we discover that the current implementations of the framework causes extra delays on the response time of up to 50ms for the Wi-Fi Direct implementation and up to 200ms for the AllJoyn implementation. 
Tool Traceview by Android helped us understand that the delays are mainly due to the serialization and deserialization of message contents. 
Most of the time spent in an SPF invocation is spent on parsing the received message, which is serialized as a JSON object. 
This is caused by the internal garbage collection that depends on the implementation of the \emph{gson}\footnote{\url{https://code.google.com/p/google-gson/}} library that we have used for the serialization. 
The difference between the response times of the two configurations with the SPF is the following: the WiFi Direct- based implementation performs the JSON serialization and deserialization within the middleware, while the AllJoyn-based implementation does it on top of the middleware.

Based on the same ping service described above, we also performed an additional experiment to evaluate how the framework is able to handle concurrent remote invocations that are required especially in scenarios where multiple user devices access at the same time the same service: for example, one of those provided by a device associated with a smart space, as discussed in the scenarios of chapter \ref{chapter3}. 
In this case, we found that the framework suffers scalability problems due to intrinsic limitations imposed by the Android IPC (Inter Process Communication) framework. 
Indeed, as described in the API reference documentation\footnote{\url{http://developer.android.com/reference/android/os/TransactionTooLargeException.html}}, the arguments and return value of a remote procedure call are stored in a buffer that has a limited fixed size of 1Mb; this buffer is shared among all the transactions in progress for the process handling the remote invocations. 
This means that even if an individual transaction has a moderate size, it may fail because of the load of the system. 
The problem can be overcome by synchronizing remote invocations through a queue.
For the same reason, currently a single remote invocation must have a payload that does not exceed the size of the buffer imposed by Android. 

To understand the impact of this limitation we analyzed the size of social content exchanged in the web.
The usual messages and pictures used on the web and on traditional social networks do not suffer this problem. 
Therefore is possible to exchange small profile picture and even thumbnails to be shown in an activity feed.
However the problem occurs when the payload contains large amount of data, such as high-resolution pictures or videos. 
As future work we will address the performance overhead due to serialization and we will extend the framework by providing dedicated APIs for large data transfers.