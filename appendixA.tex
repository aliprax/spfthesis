\chapter{Guide to SPF Library}
\label{appendixA}
\thispagestyle{empty}

\section{Introduction}\label{sec:introduction}

This document is a guide for using the \emph{Social Proximity Framework} (\emph{SPF}) to write Android applications. More concrete samples can be found in the official repository of the project: \url{http://github.com/deib-polimi/SPF}.

\section{Overview}\label{sec:overview}
\texttt{SPF Lib} is made up by modular components that provide access to \texttt{SPF} functions. Loading a component requires an asynchronous operation during which a connection to the framework is established. 
Among these components there is one of them which is fundamental for the networking functions and it is called \texttt{SPF}. Once this component has been loaded, the framework activates the proximity middleware and thus the device becomes visible to remote users until \texttt{SPF} is released. Listing \ref{lst:spf_component} shows how this component can be loaded and used to access the framework proximity services. The other components follow a similar pattern and are discussed in the following sections.

\begin{lstlisting} [caption=Loading SPF connection., label=lst:spf_component]
SPF.load(context,new SPF.Callback(){
    void onServiceReady(SPF spfConnection){
        SPFSearch search = spf.getComponent(SPF.SEARCH);
        //Do your work here...
        //...and disconnect when finished
        spf.disconnect();
    }

    void onDisconnect(){
        //disconnection from the framework
        //the connection is not valid anymore
    }

    void onError(SPFError err){
         //handle errors here
    }
});
\end{lstlisting}

External applications must ask for permissions according to the services they want to use. Listing \ref{lst:permission_manager} shows how to use the \texttt{SPFPermissionManager} class to declare the needed permissions. Declaration must be performed before accessing the framework e.g. in your \texttt{Application} class or in the \texttt{onCreate} method of your \texttt{MainActivity}.

\begin{lstlisting} [caption=Declaring permissions., label=lst:permission_manager]
SPFPermissionManager.requirePermission(
    Permission.SEARCH_SERVICE,
    Permission.READ_LOCAL_PROFILE,
    Permission.REGISTER_SERVICES);
\end{lstlisting}

According to the SPF provider application you want access, you should provide additional configuration settings by means of the SPFInfo class. These parameters are used to create the android \texttt{ComponentName} that identifies the \texttt{Service} which offers the access to SPF.

\begin{lstlisting} [caption=Configuring SPFInfo., label=lst:spfinfo_config]
SPFInfo.PACKAGE_NAME = "my.spf.frontend.app";
SPFInfo.CLASS_NAME = "my.custom.spf.service.SPFService";
\end{lstlisting}

The configuration is required only if you are not using the front-end application provided with the framework. Follows a description of the parameters to be set:

\begin{description}
	\item[PACKAGE\_NAME] The package name of the SPF provider application. 
	Use the one of the front end application, by default 
	it is set to spf official front-end i.e. \texttt{it.polimi.spf.app}.
	\item[CLASS\_NAME]The class name of the service that offers SPF interfaces. 
	By default it is set to \texttt{it.polimi.spf.framework.local\-.SPF\-Service}, 
	modify the constant according to the service registered in the front-end application.
\end{description}

\section{SPFSearch API}\label{sec:spf_search}
This section describes how an application can discover remote instances of \emph{SPF} according to customizable queries. A search works by broadcasting a query signal to remote devices in proximity; if it matches their user profiles, a response is sent back along with some basic information about the user. To access SPFSearch API, you must ask for \texttt{SEARCH\_SERVICE} permission.

\subsection{Defining a query}
\emph{SPF Search} requires the creation of a \texttt{SPFQuery} object, that can be created with a builder defined in its class.
A query can be configured with different types of parameters:

\begin{description}
	\item[Profile field value] returns all the users that contain the value specified in the query for the given profile field.
	\item[Tag] returns all the users that contain the specified word in any profile fields.
	\item[App Identifier] returns the users that installed the given application. The app identifier is the application package name.
\end{description}

The query is satisfied if all the provided parameters are matched; thus, if you want to search for all the women related to a tag ``Android'', you should specify ``female'' as value of gender profile field and ``android'' as a tag. Providing an empty query returns all the instances of \emph{SPF} in proximity. Listing \ref{lst:query_building} shows an example.

\begin{lstlisting}[caption=Building a query, label=lst:query_building]
SPFQuery query = new SPFQuery.Builder()
    .setAppIdentifier("com.spf.demo.search")
    .setProfileField(ProfileField.GENDER,"female")
    .setTag("android")
    .build();
\end{lstlisting}

\subsection{Starting a search}

Once you have created a query, you have to define a \texttt{SearchDescriptor}, which allows configure the behavior of the search operation. In detail, the \texttt{SearchDescriptor} is made up by the \texttt{SPFQuery} object and two additional parameters: the number of signals to be sent, and the time interval between them, the latter measured in milliseconds. 
Finally, you must acquire a connection to the \emph{SPF} proximity service and ask for \texttt{SPFSearch}. Listing \ref{lst:search_start} provides an example.

\begin{lstlisting}[caption=Starting a search, label=lst:search_start]
long interval = 5000; // 5 seconds
int numberOfSignals = 5;
SPFSearchDescriptor descriptor = 
    new SPFSearchDescriptor(interval, numberOfSignals, query);
SPFSearch search = spfConnection.getComponent(SPF.SEARCH);
search.startSearch(SEARCH_TAG, descriptor, mSearchCallback);
\end{lstlisting}

The \texttt{startSearch} method requires two more parameters. The String  \texttt{SEARCH\_TAG} is simply a string that identifies the request and can be used to control a running query; when you provide an identifier already owned by another running request, it is automatically stopped by the framework. \texttt{SPFSearchCallback} contains the callbacks to be executed when the framework delivers the results to your application:

\begin{description}
	\item[onSearchStart]notifies that the search has begun. This is the right moment to show the user a progress bar.
	\item[onPersonFound]is called when a search result is collected. It delivers information about the discovered user in a \texttt{SPFPerson} object. The method \texttt{getBaseInfo()} allows to retrieve the identifier and the name of the user.
	\item[onPersonLost]is called when a previously found user has been lost.
	\item[onSearchStop]is called when the search operation has stopped and no further result will be delivered.
	\item[onSearchError]is called when an error occurs while executing the search.
\end{description}

If you want to retrieve a stub of a spf instance whose identifier is known, you should use the \texttt{lookup} method; if the remote instance is reachable, it returns a \texttt{SPFPerson} object that can be used with other \emph{SPF} services.

\section{SPFProfile API}\label{sec:spf_profile}
SPF allows applications to read and write on a shared user profile. To access these data you must load \texttt{SPFLocalProfile}, which requires \texttt{READ\_LOCAL\_PROFILE} and \texttt{WRITE\_LOCAL\_PROFILE} permissions, according to the needed use. Be aware that the two are independent, thus if your application wants to read and write the profile, it must require both the permissions.

\begin{comment}
Here we can add a sub section about the ProfileField class and list all the available fields.
\end{comment}

\subsection{Reading local profile}
Once the component is loaded, you can read or write the profile specifying the values you want to access. The class \texttt{ProfileField} holds the definitions of profile fields. Listing \ref{lst:lib_profile_read} shows how a read operation can be performed.

\begin{lstlisting}[caption=Reading from local profile., label=lst:lib_profile_read]
SPFLocalProfile.load(context, new SPFLocalProfile.Callback() {
    public void onServiceReady(SPFLocalProfile spfLocalProfile) {
        ProfileFieldContainer container = spfLocalProfile.getValueBulk( 
            ProfileField.IDENTIFIER,
            ProfileField.DISPLAY_NAME,
            ProfileField.PHOTO,
            ProfileField.BIRTHDAY );
            
        String uid = container.getFieldValue(ProfileField.IDENTIFIER);
        String name = container.getFieldValue(ProfileField.DISPLAY_NAME);
        Bitmap profilePic = container.getFieldValue(ProfileField.PHOTO);
        Date birthday = container.getFieldValue(ProfileField.BIRTHDAY);
        spfLocalProfile.disconnect();
     }

     public void onError(SPFError spfError) {...}

     public void onDisconnect() {...}
});
\end{lstlisting}

\subsection{Writing local profile}
Writing profile fields works in a similar way. You have to use a \texttt{ProfileFieldContainer} by creating a new one, or by reusing the one that is returned as result of a read operation. Old values are overwritten by the new ones contained in the data structure\footnote{\texttt{ProfileFieldContainer} keeps track of the modified values: only the modified fields are actually written. Thus you can use this data structure as a model for profile editing activities. For more details see the class documentation.}. 

\begin{lstlisting}[caption=Writing a profile fields, label=profile_write_lst]
ProfileFieldContainer container = new ProfileFieldContainer();
Date birthDate = new Date();
String[] emails = new String[]{"an.email@email.com","another@email.com"};
container.setFieldValue(ProfileField.BIRTHDAY, birthDate);
container.setFieldValue(ProfileField.EMAILS, emails);
spfLocalProfile.setValueBulk(container);
\end{lstlisting}

\subsection{Reading remote profile}
Application can read profiles of remote users; while the interface is similar to the one of the local profile, accessing remote devices requires the loading of the \texttt{SPF} component as described in section \ref{sec:overview}. Listing \ref{lst:lib_profile_remote} shows the three steps procedure needed to access a remote profile: obtaining the \texttt{SPFRemoteProfile} component, creating a stub from a \texttt{SPFPerson} instance\footnote{\texttt{SPFPerson} instances can be obtained from a search or a lookup. See section \ref{sec:spf_search} for details.} and finally, asking for the desired profile fields.

\begin{lstlisting}[caption=Reading a remote profile, label=lst:lib_profile_remote]
SPFRemoteProfile remPr = spfConnection.getComponent(REMOTE_PROFILE);
remPr.getProfileOf(spfPerson).getValueBulk(...);
\end{lstlisting}

\section{SPFService API}\label{sec:spf_service}
This section describe and provides examples about SPF service API. According to the intended use, your application should require the following permissions: \texttt{REGISTER\_SERVICES}, \texttt{EXECUTE\_LOCAL\_SERVICES}, \texttt{EXECUTE\_REMOTE\_SERVICES}.

\subsection{Defining a service}
The \texttt{@ServiceInterface} annotation can be used on a standard Java interface to define and configure a \emph{SPF Service}. In the annotation you must specify:

\begin{description}
	\item[app]The identifier of the application (i.e. its package name).
	\item[name]The name of the service that you are defining.
	\item[description]A textual description of the service to be shown to the user. Should contain useful information to let the user recognize the service.
	\item[version]The version of the service. 
\end{description}

\begin{lstlisting}[caption=Defining a service interface.,label=lst:service_interface]
package com.example.chat;
import it.polimi.spf.lib.services.ServiceInterface;

@ServiceInterface(
    app = "com.example.chat",
    name = "SPFChatDemo Proximity",
    description = "Service that allows users to communicate",
    version = "0.1",
)
public interface ProximityService {

    void sendMessage(String senderId, String message) 
        throws ServiceInvocationException;

    void sendPoke(String senderId)
        throws ServiceInvocationException;

}
\end{lstlisting}

Listing \ref{lst:service_interface} shows an example of how the annotation should be used. To provide an implementation of the service you have to create a class that extends \texttt{SPFServiceEndpoint} and implements the previously defined interface.
Pay attention to declare the \texttt{ServiceInvocationException} in each remote method signature; the omission will prevent to recover from errors and will cause a runtime exception when one of these occurs.

\subsection{Registering a service}
Once the service interface and its implementation are defined, the application needs to register the service. To do so, you must load \texttt{SPFServiceRegistry} as shown in Listing \ref{lst:service_registry}.

\begin{lstlisting}[caption=Registering a service.,label=lst:service_registry]
SPFServiceRegistry.load(context, new SPFServiceRegistry.Callback() {

    public void onServiceReady(SPFServiceRegistry serviceRegistry) {
        serviceRegistry.registerService(
            ProximityService.class, ProximityServiceImpl.class);
        serviceRegistry.disconnect();
    }

    public void onError(SPFError spfError) {...}

    public void onDisconnect() {...}
});
\end{lstlisting}

Eventually, you have to register your subclass of \texttt{SPFServiceEndpoint}\footnote{\texttt{SPFServiceEndpoint} is an abstract class that extends a common Android \texttt{Service}.} in the manifest file of your application.

\subsection{Executing a service}
Executing a service is a two step operation that requires the creation of a stub and the method invocation. \texttt{SPFServiceExecutor} is the component that allows you to create the service stub given the instance of \texttt{SPFPerson} referring to the remote user, the interface of the service that you want to invoke, and eventually the class loader. Listing \ref{lst:service_remote_execution} shows how the previously defined service can be executed. 
\begin{lstlisting}[caption=Executing a service.,label=lst:service_remote_execution]
SPFServiceExecutor executor = spf.getComponent(SPF.SERVICE_EXECUTION);
try {
    ProximityService myService = executor.createStub(
        spfPerson, ProximityService.class, getClassLoader());
    myservice.sendMessage(myIdentifier, message);
} catch (ServiceInvocationException e) {
    Log.e(TAG, "Cannot send message", e);
}
\end{lstlisting}

\subsection{Supported data types}
All primitive Java types are supported, along with collections and arrays. Moreover, the library supports serialization and deserialization of custom classes and their respective arrays, if they do not have circular references.
Limitations on the request size depends on the proximity middleware in use\footnote{E.g. on AllJoyn the limit size is 100Kb.} and on the Android Binder buffer used for inter-process communication\footnote{It has a limited fixed size of 1Mb, shared by all the transactions in progress for the process. More details can be found on Android documentation following this link: \url{http://developer.android.com/reference/android/os/TransactionTooLargeException.html}.}.

\section{SPFNotification API}\label{sec:spf_notification}
\texttt{SPF} allows users to advertise selected fields of their own profile. This functionality can be configured from the framework front-end, and offers a more efficient solution for long running searches that work in background. The actual implementation depends on the networking middleware in use, but basically it consists in broadcasting a set of selected profile fields that is received by other \texttt{SPF} instances. External applications may react to the reception of these information by defining triggers. To access notification services, applications must declare \texttt{NOTIFICATION\_SERVICES} permission.

\subsection{Triggers and actions}
\texttt{SPFTrigger} is the abstraction that represents the ``if\ldots then\ldots'' rule, and can be used to react to the reception of an advertised profile. A trigger is made up by two components: a query and an action. The query is the same object defined in section \ref{sec:spf_search}, and it allows to define searches for specific profile fields as well as more general queries.
There exist two types of action that can be declared in a trigger:

\begin{description}
	\item[\texttt{SPFActionIntent}] broadcasts an Android \texttt{Intent} that can be received from the application that registered the trigger; This \texttt{SPFAction} requires an action name for the \texttt{Intent} and provides information about the remote \texttt{SPF} instance that activated the trigger.
	\item[\texttt{SPFActionMessage}] sends a message to the remote user that has activated the trigger. The framework front-end will notify the user about the received message.
\end{description}

Moreover, applications can specify whether a trigger is to be activated multiple times by the same remote instance or not. In the first case you should provide a \texttt{sleepPeriod}, a time interval in milliseconds during which the trigger is not activated on a already targeted user.

Listing \ref{lst:trigger_intent} shows the creation of a trigger with a \texttt{SPFActionIntent} to count and log all the encountered instances of \texttt{SPF}. Notice that in this case no \texttt{sleepPeriod} is specified: in this way the intent is broadcast only once for each individual instance. Finally listing \ref{lst:trigger_receiver} shows the implementation of a \texttt{BroadcastReceiver} that uses the information provided by the framework by mean of the \texttt{Intent}.

\begin{lstlisting}[caption=Trigger for logging instances.,label=lst:trigger_intent]
SPFQuery query = new SPFQuery.Builder().build();
SPFActionIntent action = new SPFActionIntent("com.trigger.example.COUNTER");
String name = "People counter";
SPFTrigger trigger = new SPFTrigger(name, query, action);
\end{lstlisting}

\begin{lstlisting}[caption=BroadcastReceiver for logging instances.,label=lst:trigger_receiver]
public class PeopleCounter extends BroadcastReceiver{

    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("com.trigger.example.COUNTER"){
            String triggerName = intent
                .getStringExtra(SPFActionIntent.ARG_STRING_TRIGGER_NAME);
            String targetId = intent
                .getStringExtra(SPFActionIntent.ARG_STRING_TARGET);
            String displayname = intent
                .getStringExtra(SPFActionIntent.ARG_STRING_DISPLAY_NAME);		
            Log.d(TAG, "Found user: " + displayName + " id: " + targetId);
    }

}
\end{lstlisting}

Listing \ref{lst:trigger_message} shows an example of a welcome message sent once in a day. Here a \texttt{sleepPeriod} is set and a \texttt{SPFActionMessage} is used.

\begin{lstlisting}[caption=Trigger for welcome messages.,label=lst:trigger_message]
SPFQuery query = new SPFQuery.Builder().build();
String object = "Welcome";
String message = "Welcome in this beautiful Social Smart Space!";
SPFActionMessage action = new SPFActionMessage(title,message);
SPFTrigger trigger = new SPFTrigger(name, query, action, DAY_MILLIS);
\end{lstlisting}

\subsection{Registering triggers}
To access \texttt{SPF} notification services you have to load \texttt{SPFNotification} component. Once you have obtained an instance of this object you can call the methods that allows to register and modify the triggers.

\begin{description}
	\item[\texttt{saveTrigger(SPFTrigger trigger)}] registers the trigger given as parameter and assigns an id.
	\item[\texttt{listTriggers()}] returns the list of trigger that have been registered by the application.
	\item[\texttt{getTrigger(long id)}] returns the trigger with the specified id
	\item[\texttt{deleteTrigger(long id)}] deletes the trigger with the specified id
	\item[\texttt{deleteAllTriggers()}] deletes all the triggers that have been registered by the application.
\end{description}

\begin{lstlisting}[caption=Reading saved triggers.,label=lst:trigger_management]
SPFNotification.load(context,new SPFNotification.Callback(){

    public void onServiceReady(SPFNotification spfNotification){
        List<SPFTrigger> triggers = spfNotification.listTriggers();
        ...
        spfNotification.disconnect();
    }

    public void onDisconnect(){...}

    public void onError(SPFError err){...}

});
\end{lstlisting} 

\section{SPFActivities}\label{sec:spf_activities}
\texttt{SPFActivity} is an interoperable key-value data container that describes a potential or completed social action. As discussed in the next section it is designed to speed up the development of standard services and to ease the integration of different applications.

\subsection{Data structure}
Each activity features a set of standard fields:

\begin{description}
	\item[Verb] a string that identifies the type of the social action
	\item[SenderIdentifier and SenderDisplayName]: details about the user who
	performed the activity;
	\item[ReceiverIdentifier and ReceiverDisplayName]: details about the user target of
	the activity.
\end{description}

The personal details of the sender and the receiver are automatically injected by the
framework without the need of additional lookups; activities may also contain a map of fields needed to describe the action.

Activities can be used as parameters in methods of service interfaces, removing the
need of additional parameters to pass context information like the name of the
sender of a chat message.

\begin{lstlisting}[caption=Using an activity for a chat.,label=lst:activity_definition]
public void onMessageReceived(SPFActivity message) {
    ChatStorage storage = ChatDemoApp.get().getChatStorage();
    String senderId = message.get(SPFActivity.SENDER_IDENTIFIER);
    String senderName = message.get(SPFActivity.SENDER_DISPLAY_NAME);
    Conversation c = storage.findConversationWith(senderId);
    if (c == null) {
        c = new Conversation();
        c.setContactIdentifier(senderId);
        c.setContactDisplayName(senderName);
        storage.saveConversation(c);
    }

    Message m = new Message();
    m.setSenderId(c.getContactIdentifier());
    m.setText(message.get(ProximityService.MESSAGE_TEXT));
    m.setRead(false);
    storage.saveMessage(m, c);
}
\end{lstlisting} 

\subsection{Verbs routing}
\emph{SPF} implements an automatic routing service that dispatches activities to applications according to verbs. To access this functionality, you need to declare a service as a consumer of one or more activity verbs. Since more than one service can be consumer of a given verb, the user of SPF can select which of them is the default one by using the framework front-end. Sending activities to external applications, both local and remote, is possible through the method \texttt{sendActivity} of \texttt{SPFServiceExecutor} class. Listing \ref{lst:lib_verb_consumer} and \ref{lst:activity_sending} shows respectively the definition of a service  and the invocation of a method based on the verb routing mechanism.
\begin{lstlisting}[caption=Defining a verb consumer.,label=lst:lib_verb_consumer]
@ServiceInterface(
    app = "it.polimi.spf.demo.chat",
    name = "SPFChatDemo Proximity",
    description = "Service that allows users to communicate",
    version = "0.1",
    consumedVerbs = {
        ProximityService.POKE_VERB,
        ProximityService.MESSAGE_VERB }
)
public interface ProximityService {

    public static final String POKE_VERB = "poke";
    public static final String CHAT_VERB = "chat";

    @ActivityConsumer(verb = POKE_VERB)
    void onPokeReceived(SPFActivity poke);

    @ActivityConsumer(verb = CHAT_VERB)
    void onMessageReceived(SPFActivity message);
}
\end{lstlisting}

\begin{lstlisting}[caption=Sending an activity.,label=lst:activity_sending]
SPFActivity message = new SPFActivity(ProximityService.CHAT_VERB);
message.put("text", text);
//spfPerson is an instance of SPFPerson retrieved from a search or a lookup
spfPerson.sendActivity(spfConnection, message); 
\end{lstlisting} 
