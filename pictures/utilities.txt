//Conversion of pdf to a different version format with ghostssrcipt
gs -sDEVICE=pdfwrite -dNOPAUSE -dBATCH -dSAFER -dCompatibilityLevel=1.4 -sOutputFile=output.pdf input.pdf